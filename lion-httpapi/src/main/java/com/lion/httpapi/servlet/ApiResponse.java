package com.lion.httpapi.servlet;

import java.io.Serializable;

/**
 * API返回
 * 
 * @author liyongyao
 *
 */
public class ApiResponse implements Serializable {

	private static final long serialVersionUID = 3871244063057535568L;
	private String path;
	private String message;
	private int code;
	private Object data;
	private long timestamp = System.currentTimeMillis();

	public ApiResponse(String path, Object[] msg, Object data) {
		this(path, msg[1].toString(), Integer.valueOf(msg[0].toString()), data);
	}

	public ApiResponse(String path, Object[] msg) {
		this(path, msg[1].toString(), Integer.valueOf(msg[0].toString()));
	}

	public ApiResponse(String path, String message, int code) {
		this(path, message, code, null);
	}

	public ApiResponse(String path, String message, int code, Object data) {
		this.path = path;
		this.message = message;
		this.code = code;
		this.data = data;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "NettyResponse [path=" + path + ", message=" + message
				+ ", code=" + code + ", data=" + data + ", timestamp="
				+ timestamp + "]";
	}

}
