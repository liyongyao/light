package com.lion.httpapi.servlet;

import java.io.Serializable;

import com.lion.httpapi.tools.JsonTools;
/**
 * API请求
 * @author liyongyao
 *
 */
public class ApiRequest implements Serializable {
	private static final long serialVersionUID = 5604277160168347365L;
	private String path;
	private String clientName;
	private Object data;
	private long timestamp = System.currentTimeMillis();
	private long timeout = 5*30 * 1000;

	/**
	 * 精简的构造方法
	 * 
	 * @param path
	 *            调用路径
	 * @param clientName
	 *            客户端名称
	 */
	public ApiRequest(String path, String clientName) {
		this.path = path;
		this.clientName = clientName;
	}

	/**
	 * 带数据的请求
	 * 
	 * @param path
	 *            调用路径
	 * @param clientName
	 *            客户端名称
	 * @param data
	 *            json数据
	 */
	public ApiRequest(String path, String clientName, Object data) {
		this.path = path;
		this.clientName = clientName;
		this.data = data;
	}

	/**
	 * 带数据、自定义超时时间的请求
	 * 
	 * @param path
	 *            调用路径
	 * @param clientName
	 *            客户端名称
	 * @param data
	 *            json数据
	 * @param timeOut
	 *            超时时间（毫秒）
	 */
	public ApiRequest(String path, String clientName, Object data,
			long timeOut) {
		this.path = path;
		this.clientName = clientName;
		this.data = data;
		this.timeout = timeOut;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Object getData() {
		return data;
	}
	
	public <T>T getData(Class<T> clazz){
		if(data!=null){
			if(getData() == null){
				return null;
			}
			return JsonTools.jsonToBean(getData().toString(), clazz);
		}else{
			return null;
		}
	}

	public void setData(Object data) {
		this.data = data;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	@Override
	public String toString() {
		return JsonTools.beanToJson(this);
	}

}
