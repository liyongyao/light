package com.lion.httpapi.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.lion.httpapi.servlet.ActionMethod;

/**
 * http请求路径
 * 
 * @author 李永曜 2015年9月28日
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HttpPath {
	String value() default "";
	ActionMethod[] method() default {ActionMethod.GET,ActionMethod.POST};
}
