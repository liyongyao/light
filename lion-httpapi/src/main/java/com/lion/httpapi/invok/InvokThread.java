package com.lion.httpapi.invok;

import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;

import com.lion.httpapi.servlet.ApiRequest;
import com.lion.httpapi.servlet.ApiResponse;
import com.lion.httpapi.tools.ExceptionTools;

public class InvokThread implements Runnable {
	private CountDownLatch cdl;
	private Object o;
	private Method m;
	private ApiRequest request;
	private ApiResponse response;
	
	public InvokThread(Object o, Method m, ApiRequest request,ApiResponse response, CountDownLatch cdl){
		this.cdl = cdl;
		this.o = o;
		this.m = m;
		this.request = request;
		this.response = response;
	}
	@Override
	public void run() {
		try{
			m.invoke(o, request,response);
		} catch (Exception e) {
			StringBuilder sb = ExceptionTools.cover(e);
			response.setCode(500);
			response.setMessage("系统错误");
			response.setData(sb.toString());
		}finally{
			cdl.countDown();
		}
		
	}
	public ApiResponse getResponse() {
		return response;
	}

}
