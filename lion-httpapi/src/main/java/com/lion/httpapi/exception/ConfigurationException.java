package com.lion.httpapi.exception;

public class ConfigurationException extends RuntimeException {
	private static final long serialVersionUID = -7436287694198444056L;
	private String message;

	public ConfigurationException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
