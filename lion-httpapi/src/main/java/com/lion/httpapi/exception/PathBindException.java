package com.lion.httpapi.exception;

public class PathBindException extends RuntimeException {

	private static final long serialVersionUID = -3850214797144053614L;

	private String message;

	public PathBindException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
