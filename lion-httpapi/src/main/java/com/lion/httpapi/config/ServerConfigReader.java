package com.lion.httpapi.config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
/**
 * 解析Server配置
 * @author liyongyao
 *
 */
public class ServerConfigReader {
	
	@SuppressWarnings("unchecked")
	public static Map<String, String> readConfig() {
		Map<String, String> result = new HashMap<String, String>();
		InputStream in = ServerConfigReader.class.getResourceAsStream("/server-config.xml");
		SAXReader reader = new SAXReader();
		try {
			Document doc = reader.read(in);
			Element root = doc.getRootElement();
			List<Element> list = root.element("params").elements("param");
			for (Element e : list) {
				result.put(e.attributeValue("name"), e.attributeValue("value"));
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) {
		Map<String, String> map = ServerConfigReader.readConfig();
		System.out.println(map);
	}
}
