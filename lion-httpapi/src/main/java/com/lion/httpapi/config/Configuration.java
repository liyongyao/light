package com.lion.httpapi.config;

import java.util.Map;

import com.lion.httpapi.tools.StringTools;

public class Configuration {
	private static Configuration config;
	private Map<String, String> map;

	private Configuration() {
		reloadConfig();
	}
	
	public void reloadConfig(){
		map = ServerConfigReader.readConfig();
	}

	public static Configuration getInstance() {
		if (config == null) {
			config = new Configuration();
		}
		return config;
	}

	public String get(String key) {
		return map.get(key);
	}
	
	/**
	 * 若取不到值返回null
	 * @param key
	 * @return
	 */
	public Integer getInt(String key) {
		String value = map.get(key);
		if (StringTools.isIntegerNumber(value)) {
			return Integer.valueOf(value);
		}
		return null;
	}

	public int getInt(String key, int defaultValue) {
		Integer value = getInt(key);
		if(value == null){
			return defaultValue;
		}
		return value;
	}
	
	public long getLong(String key,long defaultValue){
		String value = map.get(key);
		if (StringTools.isIntegerNumber(value)) {
			return Long.valueOf(value);
		}
		return defaultValue;
	}
	

	public Long getLong(String key) {
		String value = map.get(key);
		if (StringTools.isIntegerNumber(value)) {
			return Long.valueOf(value);
		}
		return null;
	}
}
