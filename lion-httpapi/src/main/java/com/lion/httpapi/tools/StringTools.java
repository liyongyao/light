package com.lion.httpapi.tools;

import java.util.regex.Pattern;
/**
 * 字符串工具
 * @author liyongyao
 *
 */
public class StringTools {
	public static final String SPLIT = "/";

	public static boolean isNull(String str) {
		if (str == null || str.trim().equals("")) {
			return true;
		}
		return false;
	}

	public static boolean isNumber(String text) {
		String regex = "^(\\d+)\\.?(\\d+)$";
		return regexMatch(text, regex);
	}

	public static boolean isIntegerNumber(String text) {
		String regex = "^\\d+$";
		return regexMatch(text, regex);
	}

	public static boolean regexMatch(String text, String regex) {
		if(isNull(text)){
			return false;
		}
		return Pattern.compile(regex).matcher(text).find();
	}
}
