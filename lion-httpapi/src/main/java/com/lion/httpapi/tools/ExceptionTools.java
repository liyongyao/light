package com.lion.httpapi.tools;

public class ExceptionTools {

	public static StringBuilder cover(Exception e) {
		StringBuilder sb = new StringBuilder();
		sb.append(e.toString()).append("\n");
		for (StackTraceElement stc : e.getStackTrace()) {
			sb.append(stc.toString()).append("\n");
		}
		return sb;
	}
}
