package com.lion.httpapi.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * 线程池管理类
 * 
 * @author liyongyao
 *
 */
public class ThreadManager {
	private static ThreadManager threadManager;

	private Map<String, ThreadPool> threadMap = new HashMap<String, ThreadPool>();

	private ThreadManager() {

	}

	public static ThreadManager getThreadManager() {
		if (threadManager == null) {
			threadManager = new ThreadManager();
		}
		return threadManager;
	}

	/**
	 * 获得一个线程池，如果不存在则创建，最大线程数量为maxSize
	 * 
	 * @param threadPoolName
	 * @param maxSize
	 * @return
	 */
	public ThreadPool getThreadPool(String threadPoolName, int maxSize) {
		ThreadPool threadPool = threadMap.get(threadPoolName);
		if (threadPool == null) {
			threadPool = new ThreadPool(maxSize);
			threadMap.put(threadPoolName, threadPool);
		}
		return threadPool;
	}

	/**
	 * 获得一个线程池，如果不存在则创建，不限制大小
	 * 
	 * @param threadPoolName
	 * @param maxSize
	 * @return
	 */
	public ThreadPool getThreadPool(ThreadPoolType threadType) {
		return getThreadPool(threadType.name(), 0);
	}

	/**
	 * 获得一个线程池，如果不存在则创建，最大线程数量为maxSize
	 * 
	 * @param threadPoolName
	 * @param maxSize
	 * @return
	 */
	public ThreadPool getThreadPool(ThreadPoolType threadType, int maxSize) {
		return getThreadPool(threadType.name(), maxSize);
	}

}
