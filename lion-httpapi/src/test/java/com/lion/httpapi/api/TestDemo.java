package com.lion.httpapi.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.lion.httpapi.HttpClient;
import com.lion.httpapi.HttpServer;
import com.lion.httpapi.config.Configuration;
import com.lion.httpapi.tools.JsonTools;

public class TestDemo {
    @Test
	public void doTest(){
		int port = Configuration.getInstance().getInt("server.netty.port");
		HttpClient client = new HttpClient("demo", "127.0.0.1", port);
		try {
			//1.简单传值请求
			DemoModel model = new DemoModel();
			model.setId(1);
			model.setName("world");
			String response = client.doPost("/demo/helloString", JsonTools.beanToJson(model), null);
			System.out.println(response);
			//2.map传值请求
			Map<String,String> params = new HashMap<String, String>();
			params.put("id", "123");
			params.put("name", "map");
			response = client.doPost("/demo/helloMap", null, params);
			System.out.println(response);
			//3.请求超时设置
			response = client.doPost("/demo/helloMap", JsonTools.beanToJson(model), 30000, null);
			System.out.println(response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Before
	public void startServer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					new HttpServer().start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
}
