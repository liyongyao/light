package com.lion.httpapi.api;

import java.util.HashMap;
import java.util.Map;

import com.lion.httpapi.annotation.HttpInterface;
import com.lion.httpapi.annotation.HttpPath;
import com.lion.httpapi.servlet.ApiRequest;
import com.lion.httpapi.servlet.ApiResponse;

@HttpInterface
@HttpPath("/demo")
public class DemoAPI {
	
	@HttpPath("/helloString")
	public void helloString(ApiRequest request, ApiResponse response) {
		DemoModel data = request.getData(DemoModel.class);
		response.setData("hello "+data.getName());
	}
	
	@HttpPath("/helloMap")
	public void helloMap(ApiRequest request, ApiResponse response) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("string", "hello world");
		map.put("int", 999);
		map.put("Object", request);
		response.setData(map);
	}
	
}
