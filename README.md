一、快速入门

1.简单配置
resources下server-config.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<root>
	<params>
		<param name="server.netty.port" value="11000" /><!--http服务监听端口-->
		<param name="server.intf.bind" value="com.lion.httpapi.api" /><!--api类包路径-->
		<param name="server.max.thread" value="100" /><!--最大并发数量，默认100-->
	</params>
</root>
```

2.查看test包下示例：

@HttpInterface 注解表示公开该类的方法
@HttpPath 注解表示路径，例如：

```
@HttpInterface
@HttpPath("/demo")
public class DemoAPI {
	
	@HttpPath("/helloString")
	public void helloString(ApiRequest request, ApiResponse response) {
		DemoModel data = request.getData(DemoModel.class);
		response.setData("hello "+data.getName());
	}

	……
}
```

void helloString(ApiRequest request, ApiResponse response) 方法的请求路径为/demo/helloString

二、约定
api方法的写法是固定的  public void methodName(ApiRequest request, ApiResponse response)

三、启动服务
new HttpServer().start();